"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultUserRepository = void 0;
const ports_1 = require("../../../app/ports");
const data_mappers_1 = require("./data-mappers");
const mongoose_repository_1 = require("../data-store/mongoose/mongoose.repository");
const domain_error_1 = require("../model/domain.error");
const inversify_1 = require("inversify");
const mongoose_1 = require("mongoose");
const persistence_types_1 = require("../persistence.types");
let DefaultUserRepository = class DefaultUserRepository extends mongoose_repository_1.MongooseRepositoryBase {
    constructor(model) {
        super(model);
        this.model = model;
    }
    findByUserId(id) {
        return super
            ._findById(id)
            .then((userModel) => {
            if (!userModel) {
                throw new domain_error_1.UserNotFoundError(`User not found. id=${id}`);
            }
            return data_mappers_1.mapModelToUser(userModel);
        })
            .catch((error) => {
            throw error;
        });
    }
    findByUsername(username) {
        const nameRegex = new RegExp(username, 'i');
        return (super
            ._findOne({ email: { $regex: nameRegex } })
            .then((userModel) => {
            if (!userModel) {
                throw new domain_error_1.UserNotFoundError(`User not found. username=${username}`);
            }
            return data_mappers_1.mapModelToUser(userModel);
        })
            .catch((error) => {
            throw error;
        }));
    }
    getPasswordForUser(username) {
        const nameRegex = new RegExp(username, 'i');
        return (super
            ._findOne({ email: { $regex: nameRegex } })
            .then((userModel) => {
            if (!userModel) {
                throw new domain_error_1.UserNotFoundError(`User not found. username=${username}`);
            }
            return userModel.password;
        })
            .catch((error) => {
            throw error;
        }));
    }
    hasUserWithEmail(username) {
        const nameRegex = new RegExp(username, 'i');
        return super
            ._findOne({ email: { $regex: nameRegex } })
            .then((docs) => !!docs);
    }
    createUser(user) {
        const newUser = new this.model({
            institution: user.institution.uniqueId,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            password: user.password,
            dataProtectionAgreed: user.dataProtectionAgreed,
            dataProtectionDate: user.dataProtectionDate,
            newsRegAgreed: user.newsRegAgreed,
            newsMailAgreed: user.newsMailAgreed,
            newsDate: user.newsDate,
        });
        return super
            ._create(newUser)
            .then((model) => ports_1.createUser(model._id.toHexString(), user.email, user.firstName, user.lastName, user.institution, user.password, user.dataProtectionAgreed, user.dataProtectionDate, user.newsRegAgreed, user.newsMailAgreed, user.newsDate, model.enabled, model.adminEnabled))
            .catch((error) => {
            throw error;
        });
    }
    updateUser(user) {
        return super
            ._update(user.uniqueId, {
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            password: user.password,
            enabled: user.isVerified(),
            adminEnabled: user.isActivated(),
            numAttempt: user.getNumberOfFailedAttempts(),
            lastAttempt: user.getLastLoginAttempt(),
            dataProtectionAgreed: user.dataProtectionAgreed,
            dataProtectionDate: user.dataProtectionDate,
            newsRegAgreed: user.newsRegAgreed,
            newsMailAgreed: user.newsMailAgreed,
            newsDate: user.newsDate,
        })
            .then((response) => {
            if (!response.ok) {
                throw new domain_error_1.UserUpdateError(`Response not OK. Unable to update user. user=${user}`);
            }
            return this.findByUserId(user.uniqueId);
        })
            .catch((error) => {
            throw error;
        });
    }
};
DefaultUserRepository = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(persistence_types_1.PERSISTENCE_TYPES.UserModel)),
    __metadata("design:paramtypes", [mongoose_1.Model])
], DefaultUserRepository);
exports.DefaultUserRepository = DefaultUserRepository;
//# sourceMappingURL=user.repository.js.map