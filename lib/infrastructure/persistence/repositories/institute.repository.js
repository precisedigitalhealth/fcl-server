"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MongooseInstituteRepository = void 0;
const ports_1 = require("../../../app/ports");
const data_mappers_1 = require("./data-mappers");
const mongoose_repository_1 = require("../data-store/mongoose/mongoose.repository");
const inversify_1 = require("inversify");
const mongoose_1 = require("mongoose");
const persistence_types_1 = require("../persistence.types");
let MongooseInstituteRepository = class MongooseInstituteRepository extends mongoose_repository_1.MongooseRepositoryBase {
    constructor(model) {
        super(model);
        this.model = model;
    }
    findByInstituteId(id) {
        return Promise.resolve(this.returnTemporaryInstitute());
    }
    retrieve() {
        return super._retrieve().then((modelArray) => {
            return modelArray.map((m) => data_mappers_1.mapModelToInstitution(m));
        });
    }
    createInstitute(institution) {
        const newInstitution = new this.model({
            state_short: institution.stateShort,
            name1: institution.name,
            city: institution.city,
            zip: institution.zip,
            phone: institution.phone,
            fax: institution.fax,
        });
        return super
            ._create(newInstitution)
            .then((model) => ports_1.createInstitution(model._id.toHexString()));
    }
    findByInstituteName(name) {
        return Promise.resolve(this.returnTemporaryInstitute());
    }
    returnTemporaryInstitute() {
        return {
            uniqueId: '5ceb924cc76307386ddbf038',
            stateShort: 'BB',
            name: 'Temporary Institute',
            addendum: 'Temporary Address',
            city: 'Berlin',
            zip: '12345',
            phone: '',
            fax: '',
            email: [],
        };
    }
};
MongooseInstituteRepository = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(persistence_types_1.PERSISTENCE_TYPES.InstitutionModel)),
    __metadata("design:paramtypes", [mongoose_1.Model])
], MongooseInstituteRepository);
exports.MongooseInstituteRepository = MongooseInstituteRepository;
//# sourceMappingURL=institute.repository.js.map