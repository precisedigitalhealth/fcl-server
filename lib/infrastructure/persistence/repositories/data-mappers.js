"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapModelToUser = exports.mapModelToInstitution = void 0;
const ports_1 = require("../../../app/ports");
function mapModelToInstitution(i) {
    const inst = ports_1.createInstitution(i._id);
    return {
        ...inst,
        ...{
            stateShort: i.state_short,
            name: i.name1,
            addendum: i.name2,
            city: i.city,
            zip: i.zip,
            phone: i.phone,
            fax: i.fax,
            email: i.email,
        },
    };
}
exports.mapModelToInstitution = mapModelToInstitution;
function mapModelToUser(model) {
    return ports_1.createUser(model._id.toHexString(), model.email, model.firstName, model.lastName, {
        uniqueId: '5ceb924cc76307386ddbf038',
        stateShort: 'BB',
        name: 'Temporary Institute',
        addendum: 'Temporary Address',
        city: 'Berlin',
        zip: '12345',
        phone: '',
        fax: '',
        email: [],
    }, model.password, model.dataProtectionAgreed, model.dataProtectionDate, model.newsRegAgreed, model.newsMailAgreed, model.newsDate, model.enabled, model.adminEnabled, model.numAttempt, model.lastAttempt);
}
exports.mapModelToUser = mapModelToUser;
//# sourceMappingURL=data-mappers.js.map