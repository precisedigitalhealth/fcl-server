"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultTokenRepository = void 0;
const mongoose_repository_1 = require("../data-store/mongoose/mongoose.repository");
const jsonwebtoken_1 = require("jsonwebtoken");
const ports_1 = require("./../../../app/ports");
const inversify_1 = require("inversify");
const mongoose_1 = require("mongoose");
const persistence_types_1 = require("../persistence.types");
let DefaultTokenRepository = class DefaultTokenRepository extends mongoose_repository_1.MongooseRepositoryBase {
    constructor(model) {
        super(model);
        this.model = model;
    }
    hasTokenForUser(user, type = ports_1.TokenType.ACTIVATE) {
        return super
            ._find({ user: user.uniqueId, type }, {}, {})
            .then((docs) => {
            return docs.length > 0;
        });
    }
    deleteTokenForUser(user, type = ports_1.TokenType.ACTIVATE) {
        return super
            ._findOne({ user: user.uniqueId, type })
            .then((token) => !!super._delete(token._id));
    }
    saveToken(token) {
        const newToken = new this.model({
            token: token.token,
            type: token.type,
            user: token.userId,
        });
        return super._create(newToken).then((res) => newToken);
    }
    getUserTokenByJWT(token) {
        return super._findOne({ token: token }).then((model) => {
            if (!model) {
                throw new jsonwebtoken_1.JsonWebTokenError(`No UserToken for JWT Token. token=${token}`);
            }
            return {
                token: model.token,
                type: model.type,
                userId: model.user,
            };
        });
    }
};
DefaultTokenRepository = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(persistence_types_1.PERSISTENCE_TYPES.TokenModel)),
    __metadata("design:paramtypes", [mongoose_1.Model])
], DefaultTokenRepository);
exports.DefaultTokenRepository = DefaultTokenRepository;
//# sourceMappingURL=token.repository.js.map