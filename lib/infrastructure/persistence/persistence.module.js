"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPersistenceContainerModule = void 0;
const persistence_types_1 = require("./persistence.types");
const inversify_1 = require("inversify");
const mongoose_model_1 = require("./data-store/mongoose/mongoose.model");
const institute_repository_1 = require("./repositories/institute.repository");
const user_repository_1 = require("./repositories/user.repository");
const token_repository_1 = require("./repositories/token.repository");
const application_types_1 = require("./../../app/application.types");
function getPersistenceContainerModule() {
    return new inversify_1.ContainerModule((bind, unbind) => {
        bind(persistence_types_1.PERSISTENCE_TYPES.UserModel).toConstantValue(mongoose_model_1.MongooseUserModel);
        bind(persistence_types_1.PERSISTENCE_TYPES.TokenModel).toConstantValue(mongoose_model_1.MongooseTokenModel);
        bind(persistence_types_1.PERSISTENCE_TYPES.InstitutionModel).toConstantValue(mongoose_model_1.MongooseInstitutionModel);
        bind(application_types_1.APPLICATION_TYPES.InstituteRepository).to(institute_repository_1.MongooseInstituteRepository);
        bind(application_types_1.APPLICATION_TYPES.UserRepository).to(user_repository_1.DefaultUserRepository);
        bind(application_types_1.APPLICATION_TYPES.TokenRepository).to(token_repository_1.DefaultTokenRepository);
    });
}
exports.getPersistenceContainerModule = getPersistenceContainerModule;
//# sourceMappingURL=persistence.module.js.map