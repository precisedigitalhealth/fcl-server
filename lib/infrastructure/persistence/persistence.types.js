"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PERSISTENCE_TYPES = void 0;
exports.PERSISTENCE_TYPES = {
    TokenModel: Symbol.for('TokenModel'),
    UserModel: Symbol.for('UserModel'),
    InstitutionModel: Symbol.for('InstitutionModel'),
};
//# sourceMappingURL=persistence.types.js.map