"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapCollectionToRepository = exports.createDataStore = void 0;
const mongoose = require("mongoose");
const Promise = require("bluebird");
const aspects_1 = require("./../../../../aspects");
const mongoose_repository_1 = require("./mongoose.repository");
const mongoose_model_1 = require("./mongoose.model");
mongoose.Promise = Promise;
class MongooseDataStore {
    constructor(connecionString) {
        mongoose.connect(connecionString).then((db) => {
            aspects_1.logger.info('Connected to DB', {
                connectionString: connecionString,
            });
            return db;
        }, (error) => {
            throw new Error(`Unable to connect to DB. connectionString=${connecionString} error=${error}`);
        });
    }
    close() {
        mongoose.connection
            .close()
            .then(() => {
            aspects_1.logger.info('Successfully closed DB');
        })
            .catch((error) => {
            throw new Error(`Unable to close DB. error=${error}`);
        });
    }
    drop(collection) {
        const drop = mongoose.connection.collection(collection).drop();
        if (drop) {
            drop.catch((error) => {
                throw new Error(`Unable to close DB. error=${error}`);
            });
        }
    }
}
function createDataStore(connectionString) {
    aspects_1.logger.info('Creating datastore');
    return new MongooseDataStore(connectionString);
}
exports.createDataStore = createDataStore;
function mapCollectionToRepository(collection) {
    switch (collection) {
        case 'institutions':
            return mongoose_repository_1.createRepository(mongoose_model_1.MongooseInstitutionModel);
        default:
            throw new Error(`Collection not found. collection=${collection}`);
    }
}
exports.mapCollectionToRepository = mapCollectionToRepository;
//# sourceMappingURL=mongoose.js.map