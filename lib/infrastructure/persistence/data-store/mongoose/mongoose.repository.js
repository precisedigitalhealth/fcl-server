"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createRepository = exports.MongooseRepositoryBase = void 0;
const mongoose_1 = require("mongoose");
const inversify_1 = require("inversify");
let MongooseRepositoryBase = class MongooseRepositoryBase {
    constructor(schemaModel) {
        this._model = schemaModel;
    }
    _create(item) {
        return this._model.create(item);
    }
    _retrieve() {
        return this._model.find({}).exec();
    }
    _update(_id, attr) {
        return this._model
            .update({ _id: this._toObjectId(_id) }, {
            ...attr,
            ...{ updated: Date.now() },
        })
            .exec();
    }
    _delete(_id) {
        return this._model
            .remove({ _id: _id })
            .exec();
    }
    _findById(_id) {
        return this._model.findById(_id).exec();
    }
    _findOne(cond) {
        return this._model.findOne(cond).exec();
    }
    _find(cond, fields, options) {
        return this._model.find(cond, options).exec();
    }
    _toObjectId(_id) {
        return mongoose_1.Types.ObjectId.createFromHexString(_id);
    }
};
MongooseRepositoryBase = __decorate([
    inversify_1.injectable(),
    __metadata("design:paramtypes", [mongoose_1.Model])
], MongooseRepositoryBase);
exports.MongooseRepositoryBase = MongooseRepositoryBase;
function createRepository(schema) {
    return new MongooseRepositoryBase(schema);
}
exports.createRepository = createRepository;
//# sourceMappingURL=mongoose.repository.js.map