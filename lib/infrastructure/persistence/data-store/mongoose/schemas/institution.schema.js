"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.institutionSchema = void 0;
const mongoose_1 = require("mongoose");
exports.institutionSchema = new mongoose_1.Schema({
    state_short: {
        type: String,
        required: true,
    },
    name1: {
        type: String,
        required: true,
    },
    name2: {
        type: String,
    },
    location: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
        required: true,
    },
    fax: {
        type: String,
    },
    zip: {
        type: String,
    },
    city: {
        type: String,
    },
    email: [
        {
            type: String,
        },
    ],
    created: {
        type: Date,
        default: Date.now,
        required: true,
    },
    updated: {
        type: Date,
        default: Date.now,
        required: true,
    },
}).pre('save', function (next) {
    if (this) {
        let doc = this;
        let now = new Date();
        if (!doc.created) {
            doc.created = now;
        }
        doc.updated = now;
    }
    next();
});
//# sourceMappingURL=institution.schema.js.map