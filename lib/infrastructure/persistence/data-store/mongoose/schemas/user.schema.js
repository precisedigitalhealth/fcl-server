"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userSchema = void 0;
const mongoose_1 = require("mongoose");
const mongooseUniqueValidator = require("mongoose-unique-validator");
exports.userSchema = new mongoose_1.Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
        uniqueCaseInsensitive: true,
    },
    password: {
        type: String,
        required: true,
    },
    enabled: {
        type: Boolean,
        default: false,
        required: true,
    },
    adminEnabled: {
        type: Boolean,
        default: false,
        required: true,
    },
    numAttempt: {
        type: Number,
        default: 0,
        required: true,
    },
    lastAttempt: {
        type: Number,
        default: Date.now(),
        required: true,
    },
    institution: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Institution',
    },
    dataProtectionAgreed: {
        type: Boolean,
        required: true,
    },
    dataProtectionDate: {
        type: Date,
        required: true,
    },
    newsRegAgreed: {
        type: Boolean,
        required: true,
    },
    newsMailAgreed: {
        type: Boolean,
        required: true,
    },
    newsDate: {
        type: Date,
        required: true,
    },
    created: {
        type: Date,
        default: Date.now,
        required: true,
    },
    updated: {
        type: Date,
        default: Date.now,
        required: true,
    },
}).pre('save', function (next) {
    if (this) {
        let doc = this;
        let now = new Date();
        if (!doc.created) {
            doc.created = now;
        }
        doc.updated = now;
    }
    next();
});
exports.userSchema.plugin(mongooseUniqueValidator);
//# sourceMappingURL=user.schema.js.map