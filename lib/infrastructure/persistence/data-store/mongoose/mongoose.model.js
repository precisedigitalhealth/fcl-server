"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MongooseUserModel = exports.MongooseTokenModel = exports.MongooseInstitutionModel = void 0;
const mongoose = require("mongoose");
const institution_schema_1 = require("./schemas/institution.schema");
const resetToken_schema_1 = require("./schemas/resetToken.schema");
const user_schema_1 = require("./schemas/user.schema");
exports.MongooseInstitutionModel = mongoose.model('Institution', institution_schema_1.institutionSchema);
exports.MongooseTokenModel = mongoose.model('ResetToken', resetToken_schema_1.tokenSchema);
exports.MongooseUserModel = mongoose.model('User', user_schema_1.userSchema);
//# sourceMappingURL=mongoose.model.js.map