"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InstituteNotFoundError = exports.UserUpdateError = exports.UserNotFoundError = exports.PersistenceError = void 0;
class PersistenceError extends Error {
    constructor(...args) {
        super(...args);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}
exports.PersistenceError = PersistenceError;
class UserNotFoundError extends PersistenceError {
    constructor(...args) {
        super(...args);
    }
}
exports.UserNotFoundError = UserNotFoundError;
class UserUpdateError extends PersistenceError {
    constructor(...args) {
        super(...args);
    }
}
exports.UserUpdateError = UserUpdateError;
class InstituteNotFoundError extends PersistenceError {
    constructor(...args) {
        super(...args);
    }
}
exports.InstituteNotFoundError = InstituteNotFoundError;
//# sourceMappingURL=domain.error.js.map