"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMailContainerModule = void 0;
const inversify_1 = require("inversify");
const mail_types_1 = require("./mail.types");
const mail_1 = require("./mail");
function getMailContainerModule(mailConfig) {
    return new inversify_1.ContainerModule((bind, unbind) => {
        bind(mail_types_1.MAIL_TYPES.MailConfiguration).toConstantValue(mailConfig);
        bind(mail_types_1.MAIL_TYPES.MailService).to(mail_1.DefaultMailService);
    });
}
exports.getMailContainerModule = getMailContainerModule;
//# sourceMappingURL=mail.module.js.map