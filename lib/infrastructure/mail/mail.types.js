"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MAIL_TYPES = void 0;
exports.MAIL_TYPES = {
    MailConfiguration: Symbol.for('MailConfiguration'),
    MailService: Symbol.for('MailService'),
};
//# sourceMappingURL=mail.types.js.map