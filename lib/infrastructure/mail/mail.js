"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultMailService = void 0;
const handlebars = require("handlebars");
const nodemailer = require("nodemailer");
const readFilePromise = require("fs-readfile-promise");
const aspects_1 = require("./../../aspects");
const ports_1 = require("../../app/ports");
const inversify_1 = require("inversify");
const mail_types_1 = require("./mail.types");
let DefaultMailService = class DefaultMailService {
    constructor(mailConfiguration) {
        this.mailConfiguration = mailConfiguration;
        this.host = 'smtp.office365.com';
        this.port = 587;
        this.viewsDir = __dirname + '/views/en/';
    }
    getMailHandler() {
        return async (data) => {
            let templateFile;
            aspects_1.logger.info(`${this.constructor.name}, handling notification type. data.type=${data.type}`);
            switch (data.type) {
                case ports_1.NotificationType.RESET_SUCCESS:
                    templateFile = await readFilePromise(this.viewsDir + 'pwnotification.html');
                    break;
                case ports_1.NotificationType.REQUEST_ACTIVATION:
                    templateFile = await readFilePromise(this.viewsDir + 'regactivation.html');
                    break;
                case ports_1.NotificationType.REQUEST_ALTERNATIVE_CONTACT:
                    templateFile = await readFilePromise(this.viewsDir + 'pwresethelp.html');
                    break;
                case ports_1.NotificationType.REQUEST_RESET:
                    templateFile = await readFilePromise(this.viewsDir + 'pwreset.html');
                    break;
                case ports_1.NotificationType.REQUEST_JOB:
                    templateFile = await readFilePromise(this.viewsDir + 'jobnotification.html');
                    break;
                case ports_1.NotificationType.REQUEST_ADMIN_ACTIVATION:
                    templateFile = await readFilePromise(this.viewsDir + 'adminactivation.html');
                    break;
                case ports_1.NotificationType.REQUEST_UNKNOWN_INSTITUTE:
                    templateFile = await readFilePromise(this.viewsDir + 'adminactivationUnknownInst.html');
                    break;
                case ports_1.NotificationType.NOTIFICATION_ADMIN_ACTIVATION:
                    templateFile = await readFilePromise(this.viewsDir + 'adminactivationNotification.html');
                    break;
                case ports_1.NotificationType.NOTIFICATION_NOT_ADMIN_ACTIVATED:
                    templateFile = await readFilePromise(this.viewsDir + 'notAdminactivationNotification.html');
                    break;
                case ports_1.NotificationType.NOTIFICATION_ALREADY_REGISTERED:
                    templateFile = await readFilePromise(this.viewsDir + 'reguserexists.html');
                    break;
                case ports_1.NotificationType.REMINDER_ADMIN_ACTIVATION:
                    templateFile = await readFilePromise(this.viewsDir + 'adminactivationReminder.html');
                    break;
                case ports_1.NotificationType.NOTIFICATION_SENT:
                    templateFile = await readFilePromise(this.viewsDir + 'sentnotification.html');
                    break;
                case ports_1.NotificationType.NEWSLETTER_AGREEMENT:
                    templateFile = await readFilePromise(this.viewsDir + 'newsagreement.html');
                    break;
                default:
                    aspects_1.logger.warn('Unknown notification type', {
                        notification: data.type,
                    });
            }
            if (templateFile) {
                this.sendMail(data.payload, templateFile.toString('utf-8'), {
                    ...data.meta,
                    ...{
                        from: this.mailConfiguration.fromAddress,
                        replyTo: this.mailConfiguration.replyToAddress,
                    },
                });
            }
        };
    }
    sendMail(templateData, templateFile, options) {
        templateData.copyrightYear = new Date().getFullYear();
        let template = handlebars.compile(templateFile);
        let result = template(templateData);
        const transporter = nodemailer.createTransport({
            host: this.host,
            port: this.port,
            auth: {
                user: 'ots-rts@outlook.com',
                pass: '#P%F3djLL?HtqPU_'
            }
        });
        const mailOptions = {
            ...options,
            ...{
                html: result,
            },
        };
        try {
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    aspects_1.logger.error(`Error sending mail. error=${error} mailSubject="${mailOptions.subject}"`);
                    return error;
                }
                else {
                    aspects_1.logger.info('Email sent', {
                        subject: mailOptions.subject,
                    });
                    aspects_1.logger.verbose(JSON.stringify(info));
                    return info;
                }
            });
        }
        catch (error) {
            aspects_1.logger.error(`Error sending mail. error=${error} mailSubject="${mailOptions.subject}"`);
            throw error;
        }
    }
};
DefaultMailService = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(mail_types_1.MAIL_TYPES.MailConfiguration)),
    __metadata("design:paramtypes", [Object])
], DefaultMailService);
exports.DefaultMailService = DefaultMailService;
//# sourceMappingURL=mail.js.map