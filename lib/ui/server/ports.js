"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var server_1 = require("./server");
Object.defineProperty(exports, "createServer", { enumerable: true, get: function () { return server_1.createServer; } });
var server_module_1 = require("./server.module");
Object.defineProperty(exports, "getServerContainerModule", { enumerable: true, get: function () { return server_module_1.getServerContainerModule; } });
//# sourceMappingURL=ports.js.map