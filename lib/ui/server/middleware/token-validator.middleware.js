"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getTokenFromHeader = exports.validateToken = void 0;
const jwt = require("express-jwt");
const enums_1 = require("../model/enums");
function validateToken(secret) {
    const whiteList = [
        '/api-docs' + enums_1.ROUTE.VERSION,
        enums_1.ROUTE.VERSION,
        enums_1.ROUTE.VERSION + '/',
        enums_1.ROUTE.VERSION + '/info',
        enums_1.ROUTE.VERSION + '/info/gdpr-date',
        enums_1.ROUTE.VERSION + '/institutes',
        enums_1.ROUTE.VERSION + '/users/login',
        enums_1.ROUTE.VERSION + '/users/gdpr-agreement',
        enums_1.ROUTE.VERSION + '/users/registration',
        enums_1.ROUTE.VERSION + '/users/reset-password-request',
        /\/v1\/users\/reset-password\/*/,
        /\/v1\/users\/verification\/*/,
        /\/v1\/users\/activation\/*/,
        /\/v1\/users\/news-confirmation\/*/,
    ];
    return jwt({
        secret,
        getToken: getTokenFromHeader,
    }).unless({
        path: whiteList,
    });
}
exports.validateToken = validateToken;
function getTokenFromHeader(req) {
    if (req.headers.authorization &&
        req.headers.authorization.split(' ')[0] === 'Bearer') {
        return req.headers.authorization.split(' ')[1];
    }
    return null;
}
exports.getTokenFromHeader = getTokenFromHeader;
//# sourceMappingURL=token-validator.middleware.js.map