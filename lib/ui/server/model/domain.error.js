"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnknownPackageConfigurationError = exports.TokenNotFoundError = exports.MalformedRequestError = exports.ServerDomainError = void 0;
class ServerDomainError extends Error {
    constructor(...args) {
        super(...args);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}
exports.ServerDomainError = ServerDomainError;
class MalformedRequestError extends ServerDomainError {
    constructor(...args) {
        super(...args);
    }
}
exports.MalformedRequestError = MalformedRequestError;
class TokenNotFoundError extends ServerDomainError {
    constructor(...args) {
        super(...args);
    }
}
exports.TokenNotFoundError = TokenNotFoundError;
class UnknownPackageConfigurationError extends ServerDomainError {
    constructor(...args) {
        super(...args);
    }
}
exports.UnknownPackageConfigurationError = UnknownPackageConfigurationError;
//# sourceMappingURL=domain.error.js.map