"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getServerContainerModule = void 0;
const inversify_1 = require("inversify");
const server_types_1 = require("./server.types");
const info_controller_1 = require("./controllers/info.controller");
const institutes_controller_1 = require("./controllers/institutes.controller");
const users_controller_1 = require("./controllers/users.controller");
const versionRoot_controller_1 = require("./controllers/versionRoot.controller");
const tokens_controller_1 = require("./controllers/tokens.controller");
function getServerContainerModule(serverCongfiguration) {
    return new inversify_1.ContainerModule((bind, unbind) => {
        bind(server_types_1.default.AppServerConfiguration).toConstantValue(serverCongfiguration);
        bind(server_types_1.default.InfoController).to(info_controller_1.DefaultSystemInfoController);
        bind(server_types_1.default.InstitutesController).to(institutes_controller_1.DefaultInstituteController);
        bind(server_types_1.default.UsersController).to(users_controller_1.DefaultUsersController);
        bind(server_types_1.default.VersionRootController).to(versionRoot_controller_1.DefaultVersionRootController);
        bind(server_types_1.default.TokensController).to(tokens_controller_1.DefaultTokensController);
    });
}
exports.getServerContainerModule = getServerContainerModule;
//# sourceMappingURL=server.module.js.map