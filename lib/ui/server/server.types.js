"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const SERVER_TYPES = {
    AppServerConfiguration: Symbol.for('AppServerConfiguration'),
    InfoController: Symbol.for('InfoController'),
    InstitutesController: Symbol.for('InstitutesController'),
    UsersController: Symbol.for('UsersController'),
    VersionRootController: Symbol.for('VersionRootController'),
    APIDocsController: Symbol.for('APIDocsController'),
    SwaggerMW: Symbol.for('SwaggerMW'),
    TokensController: Symbol.for('TokensController'),
};
exports.default = SERVER_TYPES;
//# sourceMappingURL=server.types.js.map