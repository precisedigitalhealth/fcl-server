"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AbstractController = void 0;
const enums_1 = require("../model/enums");
const inversify_express_utils_1 = require("inversify-express-utils");
let AbstractController = class AbstractController {
    jsonResponse(response, code, dto) {
        return response.status(code).json(dto);
    }
    ok(response, dto) {
        if (dto) {
            return this.jsonResponse(response, 200, dto);
        }
        else {
            return response.sendStatus(200);
        }
    }
    unauthorized(response, dto) {
        return this.jsonResponse(response, 401, dto);
    }
    clientError(response) {
        const dto = {
            code: enums_1.SERVER_ERROR_CODE.INPUT_ERROR,
            message: 'Malformed request',
        };
        return this.jsonResponse(response, 400, dto);
    }
    fail(response, message = 'An unknown error occured') {
        const dto = {
            code: enums_1.SERVER_ERROR_CODE.UNKNOWN_ERROR,
            message,
        };
        this.jsonResponse(response, 500, dto);
    }
};
AbstractController = __decorate([
    inversify_express_utils_1.controller('')
], AbstractController);
exports.AbstractController = AbstractController;
//# sourceMappingURL=abstract.controller.js.map