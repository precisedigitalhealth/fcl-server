"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultUsersController = void 0;
const aspects_1 = require("../../../aspects");
const ports_1 = require("../../../app/ports");
const abstract_controller_1 = require("./abstract.controller");
const domain_error_1 = require("../model/domain.error");
const enums_1 = require("../model/enums");
const jsonwebtoken_1 = require("jsonwebtoken");
const inversify_express_utils_1 = require("inversify-express-utils");
const inversify_1 = require("inversify");
const application_types_1 = require("./../../../app/application.types");
const domain_error_2 = require("../../../infrastructure/persistence/model/domain.error");
var USERS_ROUTE;
(function (USERS_ROUTE) {
    USERS_ROUTE["ROOT"] = "/users";
    USERS_ROUTE["RESET_PASSWORD_REQUEST"] = "/reset-password-request";
    USERS_ROUTE["RESET_PASSWORD"] = "/reset-password";
    USERS_ROUTE["LOGIN"] = "/login";
    USERS_ROUTE["VERIFICATION"] = "/verification";
    USERS_ROUTE["ACTIVATION"] = "/activation";
    USERS_ROUTE["REGISTRATION"] = "/registration";
    USERS_ROUTE["GDPR_AGREEMENT"] = "/gdpr-agreement";
    USERS_ROUTE["NEWS_CONFIRMATION"] = "/news-confirmation";
})(USERS_ROUTE || (USERS_ROUTE = {}));
let DefaultUsersController = class DefaultUsersController extends abstract_controller_1.AbstractController {
    constructor(passwordService, loginService, registrationService) {
        super();
        this.passwordService = passwordService;
        this.loginService = loginService;
        this.registrationService = registrationService;
    }
    async putResetPasswordRequest(req, res) {
        aspects_1.logger.info(`${this.constructor.name}.${this.putResetPasswordRequest.name}, Request received`);
        const resetRequest = req.body;
        const dto = {
            passwordResetRequest: true,
            email: resetRequest.email,
        };
        try {
            if (!resetRequest.email) {
                throw new domain_error_1.MalformedRequestError('Email for password reset not supplied');
            }
            await this.passwordService.requestPasswordReset({
                email: resetRequest.email,
                host: req.headers['host'],
                userAgent: req.headers['user-agent'],
            });
            aspects_1.logger.info(`${this.constructor.name}.${this.putResetPasswordRequest.name}, Response sent`);
            this.ok(res, dto);
        }
        catch (error) {
            aspects_1.logger.info(`${this.constructor.name}.${this.putResetPasswordRequest.name} has thrown an error. ${error}`);
            if (error instanceof domain_error_2.UserNotFoundError) {
                aspects_1.logger.info(`${this.constructor.name}.${this.putResetPasswordRequest.name}, Response sent`);
                this.ok(res, dto);
            }
            this.handleError(res, error);
        }
    }
    async patchResetPassword(token, req, res) {
        aspects_1.logger.info(`${this.constructor.name}.${this.patchResetPassword.name}, Request received`);
        try {
            const newPasswordRequest = req.body;
            if (!newPasswordRequest.password) {
                throw new domain_error_1.MalformedRequestError('New password or token not supplied');
            }
            await this.passwordService.resetPassword(token, newPasswordRequest.password);
            const dto = {
                passwordReset: true,
            };
            aspects_1.logger.info(`${this.constructor.name}.${this.patchResetPassword.name}, Response sent`);
            this.ok(res, dto);
        }
        catch (error) {
            aspects_1.logger.info(`${this.constructor.name}.${this.patchResetPassword.name} has thrown an error. ${error}`);
            this.handleError(res, error);
        }
    }
    async postLogin(req, res) {
        aspects_1.logger.info(`${this.constructor.name}.${this.postLogin.name}, Request received`);
        try {
            const userLoginInfo = this.mapRequestDTOToUserLoginInfo(req);
            const response = await this.loginService.loginUser(userLoginInfo);
            const dto = this.fromLoginResponseToResponseDTO(response);
            aspects_1.logger.info(`${this.constructor.name}.${this.postLogin.name}, Response sent`);
            this.ok(res, dto);
        }
        catch (error) {
            aspects_1.logger.info(`${this.constructor.name}.${this.postLogin.name} has thrown an error. ${error}`);
            this.handleError(res, error);
        }
    }
    async putGDPRAgreement(req, res) {
        aspects_1.logger.info(`${this.constructor.name}.${this.putGDPRAgreement.name}, Request received`);
        try {
            const gdprConfirmationRequest = req.body;
            if (!gdprConfirmationRequest.email) {
                throw new domain_error_1.MalformedRequestError('Email for password reset not supplied');
            }
            const response = await this.loginService.confirmGDPR(gdprConfirmationRequest);
            const dto = this.fromLoginResponseToResponseDTO(response);
            aspects_1.logger.info(`${this.constructor.name}.${this.putGDPRAgreement.name}, Response sent`);
            this.ok(res, dto);
        }
        catch (error) {
            aspects_1.logger.info(`${this.constructor.name}.${this.putGDPRAgreement.name} has thrown an error. ${error}`);
            this.handleError(res, error);
        }
    }
    async patchVerification(token, res) {
        aspects_1.logger.info(`${this.constructor.name}.${this.patchVerification.name}, Request received`);
        try {
            const username = await this.registrationService.verifyUser(token);
            const dto = {
                activation: true,
                username,
            };
            aspects_1.logger.info(`${this.constructor.name}.${this.patchVerification.name}, Response sent`);
            this.ok(res, dto);
        }
        catch (error) {
            aspects_1.logger.info(`${this.constructor.name}.${this.patchVerification.name} has thrown an error. ${error}`);
            this.handleError(res, error);
        }
    }
    async patchActivation(token, res) {
        aspects_1.logger.info(`${this.constructor.name}.${this.patchActivation.name}, Request received`);
        try {
            const username = await this.registrationService.activateUser(token);
            const dto = {
                activation: true,
                username,
            };
            aspects_1.logger.info(`${this.constructor.name}.${this.patchActivation.name}, Response sent`);
            this.ok(res, dto);
        }
        catch (error) {
            aspects_1.logger.info(`${this.constructor.name}.${this.patchActivation.name} has thrown an error. ${error}`);
            this.handleError(res, error);
        }
    }
    async postRegistration(req, res) {
        aspects_1.logger.info(`${this.constructor.name}.${this.postRegistration.name}, Request received`);
        try {
            const credentials = this.fromRequestToUserRegistration(req);
            await this.registrationService.registerUser(credentials);
            const dto = {
                registerRequest: true,
                email: credentials.email,
            };
            aspects_1.logger.info(`${this.constructor.name}.${this.postRegistration.name}, Response sent`);
            return this.ok(res, dto);
        }
        catch (error) {
            aspects_1.logger.info(`${this.constructor.name}.${this.postRegistration.name} has thrown an error. ${error}`);
            this.handleError(res, error);
        }
    }
    async patchNewsConfirmation(token, res) {
        aspects_1.logger.info(`${this.constructor.name}.${this.patchNewsConfirmation.name}, Request received`);
        try {
            const username = await this.registrationService.confirmNewsletterSubscription(token);
            const dto = {
                newsconfirmation: true,
                username,
            };
            aspects_1.logger.info(`${this.constructor.name}.${this.patchNewsConfirmation.name}, Response sent`);
            this.ok(res, dto);
        }
        catch (error) {
            aspects_1.logger.info(`${this.constructor.name}.${this.patchNewsConfirmation.name} has thrown an error. ${error}`);
            this.handleError(res, error);
        }
    }
    handleError(res, error) {
        if (error instanceof domain_error_1.MalformedRequestError) {
            this.clientError(res);
        }
        else if (error instanceof jsonwebtoken_1.JsonWebTokenError) {
            const dto = {
                code: enums_1.SERVER_ERROR_CODE.AUTHORIZATION_ERROR,
                message: 'Unauthorized request',
            };
            this.unauthorized(res, dto);
        }
        else if (error instanceof ports_1.AuthorizationError) {
            let dto = {
                code: enums_1.SERVER_ERROR_CODE.AUTHENTICATION_ERROR,
                message: 'Authentication failure',
            };
            if (error.timeToWait) {
                dto = {
                    code: enums_1.SERVER_ERROR_CODE.AUTHENTICATION_ERROR,
                    message: 'Too many failed login attempts',
                    waitTime: error.timeToWait,
                };
            }
            this.unauthorized(res, dto);
        }
        else {
            this.fail(res);
        }
    }
    fromRequestToUserRegistration(req) {
        const registrationDetail = req.body;
        try {
            if (!(registrationDetail.firstName &&
                registrationDetail.lastName &&
                registrationDetail.email &&
                registrationDetail.password &&
                registrationDetail.dataProtectionAgreed) &&
                (registrationDetail.newsRegAgreed === true ||
                    registrationDetail.newsRegAgreed === false) &&
                registrationDetail.newsMailAgreed === false) {
                throw new domain_error_1.MalformedRequestError('Registration details missing.');
            }
            const credentials = {
                firstName: registrationDetail.firstName,
                lastName: registrationDetail.lastName,
                email: registrationDetail.email,
                password: registrationDetail.password,
                dataProtectionAgreed: registrationDetail.dataProtectionAgreed,
                newsRegAgreed: registrationDetail.newsRegAgreed,
                newsMailAgreed: registrationDetail.newsMailAgreed,
                institution: '',
                userAgent: req.headers['user-agent'],
                host: req.headers['host'],
            };
            return credentials;
        }
        catch (error) {
            aspects_1.logger.error(`${this.constructor.name}.${this.fromRequestToUserRegistration.name}, unable to verify credentials during registration. error=${error}`);
            throw new domain_error_1.MalformedRequestError('Registration details invalid');
        }
    }
    mapRequestDTOToUserLoginInfo(req) {
        return {
            email: req.body.email,
            password: req.body.password,
            userAgent: req.headers['user-agent'],
            host: req.headers['host'],
            gdprDate: req.body.gdprDate,
        };
    }
    fromLoginResponseToResponseDTO(response) {
        return {
            firstName: response.user.firstName,
            lastName: response.user.lastName,
            email: response.user.email,
            token: response.token,
            instituteId: response.user.institution.uniqueId,
            gdprAgreementRequested: response.gdprAgreementRequested,
        };
    }
};
__decorate([
    inversify_express_utils_1.httpPut(USERS_ROUTE.RESET_PASSWORD_REQUEST),
    __param(0, inversify_express_utils_1.request()),
    __param(1, inversify_express_utils_1.response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], DefaultUsersController.prototype, "putResetPasswordRequest", null);
__decorate([
    inversify_express_utils_1.httpPatch(USERS_ROUTE.RESET_PASSWORD + '/:token'),
    __param(0, inversify_express_utils_1.requestParam('token')),
    __param(1, inversify_express_utils_1.request()),
    __param(2, inversify_express_utils_1.response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object, Object]),
    __metadata("design:returntype", Promise)
], DefaultUsersController.prototype, "patchResetPassword", null);
__decorate([
    inversify_express_utils_1.httpPost(USERS_ROUTE.LOGIN),
    __param(0, inversify_express_utils_1.request()), __param(1, inversify_express_utils_1.response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], DefaultUsersController.prototype, "postLogin", null);
__decorate([
    inversify_express_utils_1.httpPut(USERS_ROUTE.GDPR_AGREEMENT),
    __param(0, inversify_express_utils_1.request()), __param(1, inversify_express_utils_1.response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], DefaultUsersController.prototype, "putGDPRAgreement", null);
__decorate([
    inversify_express_utils_1.httpPatch(USERS_ROUTE.VERIFICATION + '/:token'),
    __param(0, inversify_express_utils_1.requestParam('token')),
    __param(1, inversify_express_utils_1.response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], DefaultUsersController.prototype, "patchVerification", null);
__decorate([
    inversify_express_utils_1.httpPatch(USERS_ROUTE.ACTIVATION + '/:token'),
    __param(0, inversify_express_utils_1.requestParam('token')),
    __param(1, inversify_express_utils_1.response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], DefaultUsersController.prototype, "patchActivation", null);
__decorate([
    inversify_express_utils_1.httpPost(USERS_ROUTE.REGISTRATION),
    __param(0, inversify_express_utils_1.request()), __param(1, inversify_express_utils_1.response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], DefaultUsersController.prototype, "postRegistration", null);
__decorate([
    inversify_express_utils_1.httpPatch(USERS_ROUTE.NEWS_CONFIRMATION + '/:token'),
    __param(0, inversify_express_utils_1.requestParam('token')),
    __param(1, inversify_express_utils_1.response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], DefaultUsersController.prototype, "patchNewsConfirmation", null);
DefaultUsersController = __decorate([
    inversify_express_utils_1.controller(enums_1.ROUTE.VERSION + USERS_ROUTE.ROOT),
    __param(0, inversify_1.inject(application_types_1.APPLICATION_TYPES.PasswordService)),
    __param(1, inversify_1.inject(application_types_1.APPLICATION_TYPES.LoginService)),
    __param(2, inversify_1.inject(application_types_1.APPLICATION_TYPES.RegistrationService)),
    __metadata("design:paramtypes", [Object, Object, Object])
], DefaultUsersController);
exports.DefaultUsersController = DefaultUsersController;
//# sourceMappingURL=users.controller.js.map