"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultInstituteController = void 0;
const aspects_1 = require("../../../aspects");
const abstract_controller_1 = require("./abstract.controller");
const inversify_express_utils_1 = require("inversify-express-utils");
const inversify_1 = require("inversify");
const enums_1 = require("../model/enums");
const application_types_1 = require("./../../../app/application.types");
var INSTITUTES_ROUTE;
(function (INSTITUTES_ROUTE) {
    INSTITUTES_ROUTE["ROOT"] = "/institutes";
})(INSTITUTES_ROUTE || (INSTITUTES_ROUTE = {}));
let DefaultInstituteController = class DefaultInstituteController extends abstract_controller_1.AbstractController {
    constructor(instiuteService) {
        super();
        this.instiuteService = instiuteService;
    }
    async getInstitutes(req, res) {
        aspects_1.logger.info(`${this.constructor.name}.${this.getInstitutes.name}, Request received`);
        await this.instiuteService
            .retrieveInstitutes()
            .then((institutions) => {
            const institutes = institutions.map((i) => this.fromInstituteEntityToDTO(i));
            const dto = { institutes };
            aspects_1.logger.info(`${this.constructor.name}.${this.getInstitutes.name}, Response sent`);
            this.ok(res, dto);
        })
            .catch((error) => {
            aspects_1.logger.info(`${this.constructor.name}.${this.getInstitutes.name} has thrown an error. ${error}`);
            this.handleError(res);
        });
    }
    handleError(res) {
        this.fail(res);
    }
    fromInstituteEntityToDTO(inst) {
        return {
            id: inst.uniqueId,
            short: inst.stateShort,
            name: inst.name,
            addendum: inst.addendum,
            city: inst.city,
            zip: inst.zip,
            phone: inst.phone,
            fax: inst.fax,
            email: inst.email,
        };
    }
};
__decorate([
    inversify_express_utils_1.httpGet('/'),
    __param(0, inversify_express_utils_1.request()), __param(1, inversify_express_utils_1.response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], DefaultInstituteController.prototype, "getInstitutes", null);
DefaultInstituteController = __decorate([
    inversify_express_utils_1.controller(enums_1.ROUTE.VERSION + INSTITUTES_ROUTE.ROOT),
    __param(0, inversify_1.inject(application_types_1.APPLICATION_TYPES.InstituteService)),
    __metadata("design:paramtypes", [Object])
], DefaultInstituteController);
exports.DefaultInstituteController = DefaultInstituteController;
//# sourceMappingURL=institutes.controller.js.map