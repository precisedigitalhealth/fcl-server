"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultSystemInfoController = void 0;
const aspects_1 = require("../../../aspects");
const abstract_controller_1 = require("./abstract.controller");
const inversify_express_utils_1 = require("inversify-express-utils");
const inversify_1 = require("inversify");
const enums_1 = require("../model/enums");
const server_types_1 = require("../server.types");
const domain_error_1 = require("../model/domain.error");
const application_types_1 = require("../../../app/application.types");
const pjson = require('../../../../package.json');
var INFO_ROUTE;
(function (INFO_ROUTE) {
    INFO_ROUTE["ROOT"] = "/info";
    INFO_ROUTE["GDPR_DATE"] = "/gdpr-date";
})(INFO_ROUTE || (INFO_ROUTE = {}));
let DefaultSystemInfoController = class DefaultSystemInfoController extends abstract_controller_1.AbstractController {
    constructor(configuration, configurationService) {
        super();
        this.configurationService = configurationService;
        this.supportContact = '';
        this.supportContact = configuration.supportContact;
    }
    async getSystemInfo(res) {
        aspects_1.logger.info(`${this.constructor.name}.${this.getSystemInfo.name}, Request received`);
        try {
            if (!(pjson.version && pjson.fclConfig.lastChange)) {
                throw new domain_error_1.UnknownPackageConfigurationError("Version number or date of last change can't be determined.");
            }
            const dto = {
                version: pjson.version,
                lastChange: pjson.fclConfig.lastChange,
                supportContact: this.supportContact,
            };
            aspects_1.logger.info(`${this.constructor.name}.${this.getSystemInfo.name}, Response sent`);
            this.ok(res, dto);
        }
        catch (error) {
            aspects_1.logger.info(`${this.constructor.name}.${this.getSystemInfo.name} has thrown an error. ${error}`);
            this.handleError(res);
        }
    }
    async getGDPRDate(res) {
        aspects_1.logger.info(`${this.constructor.name}.${this.getGDPRDate.name}, Request received`);
        try {
            const gdprDate = this.configurationService.getApplicationConfiguration()
                .gdprDate;
            aspects_1.logger.info(`${this.constructor.name}.${this.getGDPRDate.name}, gdprDate: ${gdprDate}`);
            if (!gdprDate) {
                throw new domain_error_1.UnknownPackageConfigurationError("Date of the data protection policy can't be determined.");
            }
            const dto = {
                gdprDate: gdprDate,
            };
            aspects_1.logger.info(`${this.constructor.name}.${this.getGDPRDate.name}, Response sent`);
            this.ok(res, dto);
        }
        catch (error) {
            aspects_1.logger.info(`${this.constructor.name}.${this.getGDPRDate.name} has thrown an error. ${error}`);
            this.handleError(res);
        }
    }
    handleError(res) {
        this.fail(res, 'Unable to retrieve system information');
    }
};
__decorate([
    inversify_express_utils_1.httpGet('/'),
    __param(0, inversify_express_utils_1.response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], DefaultSystemInfoController.prototype, "getSystemInfo", null);
__decorate([
    inversify_express_utils_1.httpGet(INFO_ROUTE.GDPR_DATE),
    __param(0, inversify_express_utils_1.response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], DefaultSystemInfoController.prototype, "getGDPRDate", null);
DefaultSystemInfoController = __decorate([
    inversify_express_utils_1.controller(enums_1.ROUTE.VERSION + INFO_ROUTE.ROOT),
    __param(0, inversify_1.inject(server_types_1.default.AppServerConfiguration)),
    __param(1, inversify_1.inject(application_types_1.APPLICATION_TYPES.ConfigurationService)),
    __metadata("design:paramtypes", [Object, Object])
], DefaultSystemInfoController);
exports.DefaultSystemInfoController = DefaultSystemInfoController;
//# sourceMappingURL=info.controller.js.map