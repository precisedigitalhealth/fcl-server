"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultVersionRootController = void 0;
const _ = require("lodash");
const aspects_1 = require("../../../aspects");
const enums_1 = require("../model/enums");
const abstract_controller_1 = require("./abstract.controller");
const inversify_express_utils_1 = require("inversify-express-utils");
const inversify_1 = require("inversify");
const server_types_1 = require("../server.types");
const openAPI = require('./../doc/openapi_v1.json');
let DefaultVersionRootController = class DefaultVersionRootController extends abstract_controller_1.AbstractController {
    constructor(configuration) {
        super();
        this.publicAPI = this.documentationRevealer(openAPI, configuration.publicAPIDoc);
    }
    async getAPIDefinition(res) {
        aspects_1.logger.info(`${this.constructor.name}.${this.getAPIDefinition.name}, Request received`);
        try {
            aspects_1.logger.info(`${this.constructor.name}.${this.getAPIDefinition.name}, Response sent`);
            this.ok(res, this.publicAPI);
        }
        catch (error) {
            aspects_1.logger.info(`${this.constructor.name}.${this.getAPIDefinition.name} has thrown an error. ${error}`);
            this.handleError(res, error);
        }
    }
    handleError(res, error) {
        this.fail(res, 'Unable to retrieve documentation');
    }
    search(term, object, found = []) {
        Object.keys(object).forEach((key) => {
            if (key === term) {
                found.push(object[key]);
                return found;
            }
            if (typeof object[key] === 'object') {
                this.search(term, object[key], found);
            }
        });
        return found;
    }
    documentationRevealer(definition, filter) {
        for (const path of Object.keys(definition.paths)) {
            if (!_.includes(Object.keys(filter), path)) {
                delete definition.paths[path];
            }
            else {
                for (const operation of Object.keys(definition.paths[path])) {
                    if (!_.includes(filter[path], operation)) {
                        delete definition.paths[path][operation];
                    }
                }
                if (_.isEmpty(definition.paths[path])) {
                    delete definition.paths[path];
                }
            }
        }
        let remainingTags = [];
        let remainingRefs;
        for (const path of Object.keys(definition.paths)) {
            for (const operation of Object.keys(definition.paths[path])) {
                if (definition.paths[path][operation].tags) {
                    remainingTags = remainingTags.concat(definition.paths[path][operation].tags);
                }
            }
        }
        remainingRefs = new Set(this.search('$ref', definition));
        const tags = definition.tags.map((t) => t.name);
        tags.forEach((tag) => {
            if (!_.includes(remainingTags, tag)) {
                _.remove(definition.tags, (t) => t.name === tag);
            }
        });
        let currentRemainingRefs = remainingRefs.size;
        let lastRemainingRefs = 0;
        const refString = '#/components';
        while (currentRemainingRefs !== lastRemainingRefs) {
            lastRemainingRefs = currentRemainingRefs;
            for (const component of Object.keys(definition.components)) {
                for (const ref of Object.keys(definition.components[component])) {
                    const reference = [refString, component, ref].join('/');
                    if (!remainingRefs.has(reference)) {
                        delete definition.components[component][ref];
                    }
                }
            }
            remainingRefs = new Set(this.search('$ref', definition));
            currentRemainingRefs = remainingRefs.size;
        }
        return definition;
    }
};
__decorate([
    inversify_express_utils_1.httpGet('/'),
    __param(0, inversify_express_utils_1.response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], DefaultVersionRootController.prototype, "getAPIDefinition", null);
DefaultVersionRootController = __decorate([
    inversify_express_utils_1.controller(enums_1.ROUTE.VERSION),
    __param(0, inversify_1.inject(server_types_1.default.AppServerConfiguration)),
    __metadata("design:paramtypes", [Object])
], DefaultVersionRootController);
exports.DefaultVersionRootController = DefaultVersionRootController;
//# sourceMappingURL=versionRoot.controller.js.map