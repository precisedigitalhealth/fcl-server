"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultTokensController = void 0;
const aspects_1 = require("../../../aspects");
const ports_1 = require("../../../app/ports");
const abstract_controller_1 = require("./abstract.controller");
const jsonwebtoken_1 = require("jsonwebtoken");
const token_validator_middleware_1 = require("../middleware/token-validator.middleware");
const inversify_express_utils_1 = require("inversify-express-utils");
const inversify_1 = require("inversify");
const enums_1 = require("../model/enums");
const application_types_1 = require("./../../../app/application.types");
var TOKENS_ROUTE;
(function (TOKENS_ROUTE) {
    TOKENS_ROUTE["ROOT"] = "/tokens";
})(TOKENS_ROUTE || (TOKENS_ROUTE = {}));
let DefaultTokensController = class DefaultTokensController extends abstract_controller_1.AbstractController {
    constructor(tokenService) {
        super();
        this.tokenService = tokenService;
    }
    async postTokens(req, res) {
        aspects_1.logger.info(`${this.constructor.name}.${this.postTokens.name}, Request received`);
        try {
            const oldToken = token_validator_middleware_1.getTokenFromHeader(req);
            if (!oldToken) {
                throw new ports_1.AuthorizationError('Invalid token');
            }
            const payload = this.tokenService.verifyToken(oldToken);
            const token = this.tokenService.generateToken(payload.sub);
            const dto = {
                refresh: true,
                token: token,
            };
            aspects_1.logger.info(`${this.constructor.name}.${this.postTokens.name}, Response sent`);
            this.ok(res, dto);
        }
        catch (error) {
            aspects_1.logger.info(`${this.constructor.name}.${this.postTokens.name} has thrown an error. ${error}`);
            this.handleError(res, error);
        }
    }
    handleError(res, error) {
        if (error instanceof ports_1.AuthorizationError ||
            error instanceof jsonwebtoken_1.JsonWebTokenError) {
            const dto = {
                message: error.message,
                code: enums_1.SERVER_ERROR_CODE.AUTHORIZATION_ERROR,
            };
            this.unauthorized(res, dto);
        }
        else {
            this.fail(res);
        }
    }
};
__decorate([
    inversify_express_utils_1.httpPost('/'),
    __param(0, inversify_express_utils_1.request()), __param(1, inversify_express_utils_1.response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], DefaultTokensController.prototype, "postTokens", null);
DefaultTokensController = __decorate([
    inversify_express_utils_1.controller(enums_1.ROUTE.VERSION + TOKENS_ROUTE.ROOT),
    __param(0, inversify_1.inject(application_types_1.APPLICATION_TYPES.TokenService)),
    __metadata("design:paramtypes", [Object])
], DefaultTokensController);
exports.DefaultTokensController = DefaultTokensController;
//# sourceMappingURL=tokens.controller.js.map