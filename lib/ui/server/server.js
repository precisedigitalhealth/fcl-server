"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createServer = exports.DefaultAppServer = void 0;
const path = require("path");
const express = require("express");
const helmet = require("helmet");
const compression = require("compression");
const cors = require("cors");
const morgan = require("morgan");
const swaggerUi = require("swagger-ui-express");
const inversify_express_utils_1 = require("inversify-express-utils");
const aspects_1 = require("./../../aspects");
const token_validator_middleware_1 = require("./middleware/token-validator.middleware");
const logging_1 = require("../../aspects/logging");
const enums_1 = require("./model/enums");
const inversify_1 = require("inversify");
const server_types_1 = require("./server.types");
let DefaultAppServer = class DefaultAppServer {
    constructor(container) {
        this.publicDir = 'public';
        this.initialise(container);
    }
    startServer() {
        const app = this.server.build();
        app.listen(app.get('port'), () => app.get('logger').info('API running', { port: app.get('port') }));
    }
    initialise(container) {
        this.server = new inversify_express_utils_1.InversifyExpressServer(container);
        const serverConfig = container.get(server_types_1.default.AppServerConfiguration);
        this.server.setConfig((app) => {
            app.use(helmet());
            app.use(compression());
            app.set('port', serverConfig.port);
            app.set('logger', aspects_1.logger);
            app.use(express.json({ limit: '50mb' }));
            app.use(express.urlencoded({
                extended: false,
            }));
            app.use((req, res, next) => {
                res.setHeader('X-Frame-Options', 'deny');
                res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
                res.setHeader('Pragma', 'no-cache');
                res.setHeader('X-XSS-Protection', '1; mode=block');
                res.setHeader('X-Content-Type-Options', 'nosniff');
                next();
            });
            app.use(cors());
            app.use(morgan(logging_1.Logger.mapLevelToMorganFormat(serverConfig.logLevel)));
            app.use(express.static(path.join(__dirname, this.publicDir)));
            app.use('/api-docs' + enums_1.ROUTE.VERSION, swaggerUi.serve, swaggerUi.setup(undefined, {
                swaggerUrl: enums_1.ROUTE.VERSION,
            }));
            app.use(enums_1.ROUTE.VERSION + '/*', token_validator_middleware_1.validateToken(serverConfig.jwtSecret));
        });
        this.server.setErrorConfig((app) => {
            app.use((err, req, res, next) => {
                if (err.status === 401) {
                    app.get('logger').warn(`Log caused error with status 401. error=${err}`);
                    res.status(401)
                        .send({
                        code: enums_1.SERVER_ERROR_CODE.AUTHORIZATION_ERROR,
                        message: err.message,
                    })
                        .end();
                }
            });
            app.get('*', (req, res) => {
                res.sendFile(path.join(__dirname, this.publicDir + '/index.html'));
            });
        });
    }
};
DefaultAppServer = __decorate([
    inversify_1.injectable(),
    __metadata("design:paramtypes", [inversify_1.Container])
], DefaultAppServer);
exports.DefaultAppServer = DefaultAppServer;
function createServer(container) {
    return new DefaultAppServer(container);
}
exports.createServer = createServer;
//# sourceMappingURL=server.js.map