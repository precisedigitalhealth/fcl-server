"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultConfigurationService = void 0;
const config = require("config");
const aspects_1 = require("./aspects");
const ports_1 = require("./ui/server/ports");
const ports_2 = require("./infrastructure/ports");
const ports_3 = require("./app/ports");
class DefaultConfigurationService {
    constructor() {
        this.loginConfigurationDefaults = {
            threshold: 5,
            secondsDelay: 300,
        };
        this.generalConfigurationDefaults = {
            logLevel: 'info',
            supportContact: '',
            jwtSecret: '',
        };
    }
    getServerConfiguration() {
        return config.get('server');
    }
    getDataStoreConfiguration() {
        return config.get('dataStore');
    }
    getMailConfiguration() {
        return config.get('mail');
    }
    getApplicationConfiguration() {
        const appConfiguration = config.get('application');
        if (!config.has('application.login')) {
            appConfiguration.login = {
                threshold: this.loginConfigurationDefaults.threshold,
                secondsDelay: this.loginConfigurationDefaults.secondsDelay,
            };
        }
        if (!config.has('application.login.threshold')) {
            appConfiguration.login.threshold = this.loginConfigurationDefaults.threshold;
        }
        if (!config.has('application.login.secondsDelay')) {
            appConfiguration.login.secondsDelay = this.loginConfigurationDefaults.secondsDelay;
        }
        return appConfiguration;
    }
    getGeneralConfiguration() {
        let generalConfiguration = config.get('general');
        if (!config.has('general')) {
            generalConfiguration = {
                logLevel: this.generalConfigurationDefaults.logLevel,
                supportContact: this.generalConfigurationDefaults
                    .supportContact,
                jwtSecret: this.generalConfigurationDefaults.jwtSecret,
            };
        }
        if (!config.has('general.logLevel')) {
            generalConfiguration.logLevel = this.generalConfigurationDefaults.logLevel;
        }
        return generalConfiguration;
    }
}
exports.DefaultConfigurationService = DefaultConfigurationService;
async function init() {
    const configurationService = new DefaultConfigurationService();
    const serverConfig = configurationService.getServerConfiguration();
    const generalConfig = configurationService.getGeneralConfiguration();
    const dataStoreConfig = configurationService.getDataStoreConfiguration();
    const appConfiguration = configurationService.getApplicationConfiguration();
    const mailConfiguration = configurationService.getMailConfiguration();
    ports_2.createDataStore(dataStoreConfig.connectionString);
    const container = aspects_1.getContainer({ defaultScope: 'Singleton' });
    container.load(ports_3.getApplicationContainerModule({
        ...appConfiguration,
        supportContact: generalConfig.supportContact,
        jwtSecret: generalConfig.jwtSecret,
    }), ports_2.getPersistenceContainerModule(), ports_1.getServerContainerModule({
        ...serverConfig,
        jwtSecret: generalConfig.jwtSecret,
        logLevel: generalConfig.logLevel,
        supportContact: generalConfig.supportContact,
    }), ports_2.getMailContainerModule(mailConfiguration));
    const application = ports_3.createApplication(container);
    const mailService = container.get(ports_2.MAIL_TYPES.MailService);
    application.addNotificationHandler(mailService.getMailHandler().bind(mailService));
    const server = ports_1.createServer(container);
    server.startServer();
    process.on('uncaughtException', (error) => {
        aspects_1.logger.error(`Uncaught Exception. error=${error}`);
        process.exit(1);
    });
}
init().catch((error) => {
    aspects_1.logger.error(`Unable to initialise application. error=${error}`);
    throw error;
});
//# sourceMappingURL=main.js.map