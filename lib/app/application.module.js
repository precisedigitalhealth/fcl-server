"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getApplicationContainerModule = void 0;
const inversify_1 = require("inversify");
const configuration_service_1 = require("./core/application/configuration.service");
const notification_service_1 = require("./core/application/notification.service");
const user_service_1 = require("./authentication/application/user.service");
const institute_service_1 = require("./authentication/application/institute.service");
const token_service_1 = require("./authentication/application/token.service");
const registration_service_1 = require("./authentication/application/registration.service");
const password_service_1 = require("./authentication/application/password.service");
const login_service_1 = require("./authentication/application/login.service");
const application_types_1 = require("./application.types");
function getApplicationContainerModule(appConfiguration) {
    return new inversify_1.ContainerModule((bind, unbind) => {
        bind(application_types_1.APPLICATION_TYPES.ApplicationConfiguration).toConstantValue(appConfiguration);
        bind(application_types_1.APPLICATION_TYPES.ConfigurationService).to(configuration_service_1.DefaultConfigurationService);
        bind(application_types_1.APPLICATION_TYPES.NotificationService).to(notification_service_1.DefaultNotificationService);
        bind(application_types_1.APPLICATION_TYPES.UserService).to(user_service_1.DefaultUserService);
        bind(application_types_1.APPLICATION_TYPES.InstituteService).to(institute_service_1.DefaultInstituteService);
        bind(application_types_1.APPLICATION_TYPES.TokenService).to(token_service_1.DefaultTokenService);
        bind(application_types_1.APPLICATION_TYPES.RegistrationService).to(registration_service_1.DefaultRegistrationService);
        bind(application_types_1.APPLICATION_TYPES.PasswordService).to(password_service_1.DefaultPasswordService);
        bind(application_types_1.APPLICATION_TYPES.LoginService).to(login_service_1.DefaultLoginService);
    });
}
exports.getApplicationContainerModule = getApplicationContainerModule;
//# sourceMappingURL=application.module.js.map