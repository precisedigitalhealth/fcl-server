"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultNotificationService = void 0;
const events_1 = require("events");
const inversify_1 = require("inversify");
const aspects_1 = require("../../../aspects");
let DefaultNotificationService = class DefaultNotificationService {
    constructor() {
        this.notificationName = 'fcl-notification';
        this.sender = new events_1.EventEmitter();
    }
    sendNotification(notification) {
        aspects_1.logger.info(`${this.constructor.name}.${this.sendNotification.name}, sending notification.  notification.type=${notification.type}`);
        this.sender.emit(this.notificationName, notification);
    }
    addHandler(handler) {
        aspects_1.logger.info(`${this.constructor.name}.${this.addHandler.name}, adding handler to notification. notificationName=${this.notificationName}`);
        this.sender.on(this.notificationName, handler);
    }
    createEmailNotificationMetaData(to, subject, cc = [], attachments = []) {
        return {
            to,
            subject,
            cc,
            attachments,
        };
    }
};
DefaultNotificationService = __decorate([
    inversify_1.injectable()
], DefaultNotificationService);
exports.DefaultNotificationService = DefaultNotificationService;
//# sourceMappingURL=notification.service.js.map