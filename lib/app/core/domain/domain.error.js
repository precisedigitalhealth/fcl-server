"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApplicationDomainError = void 0;
class ApplicationDomainError extends Error {
    constructor(...args) {
        super(...args);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}
exports.ApplicationDomainError = ApplicationDomainError;
//# sourceMappingURL=domain.error.js.map