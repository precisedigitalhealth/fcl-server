"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationType = void 0;
var NotificationType;
(function (NotificationType) {
    NotificationType[NotificationType["REQUEST_ACTIVATION"] = 0] = "REQUEST_ACTIVATION";
    NotificationType[NotificationType["REQUEST_ALTERNATIVE_CONTACT"] = 1] = "REQUEST_ALTERNATIVE_CONTACT";
    NotificationType[NotificationType["REQUEST_RESET"] = 2] = "REQUEST_RESET";
    NotificationType[NotificationType["RESET_SUCCESS"] = 3] = "RESET_SUCCESS";
    NotificationType[NotificationType["REQUEST_JOB"] = 4] = "REQUEST_JOB";
    NotificationType[NotificationType["REQUEST_ADMIN_ACTIVATION"] = 5] = "REQUEST_ADMIN_ACTIVATION";
    NotificationType[NotificationType["REQUEST_UNKNOWN_INSTITUTE"] = 6] = "REQUEST_UNKNOWN_INSTITUTE";
    NotificationType[NotificationType["NOTIFICATION_ADMIN_ACTIVATION"] = 7] = "NOTIFICATION_ADMIN_ACTIVATION";
    NotificationType[NotificationType["NOTIFICATION_NOT_ADMIN_ACTIVATED"] = 8] = "NOTIFICATION_NOT_ADMIN_ACTIVATED";
    NotificationType[NotificationType["NOTIFICATION_ALREADY_REGISTERED"] = 9] = "NOTIFICATION_ALREADY_REGISTERED";
    NotificationType[NotificationType["REMINDER_ADMIN_ACTIVATION"] = 10] = "REMINDER_ADMIN_ACTIVATION";
    NotificationType[NotificationType["NOTIFICATION_SENT"] = 11] = "NOTIFICATION_SENT";
    NotificationType[NotificationType["NEWSLETTER_AGREEMENT"] = 12] = "NEWSLETTER_AGREEMENT";
})(NotificationType = exports.NotificationType || (exports.NotificationType = {}));
//# sourceMappingURL=enums.js.map