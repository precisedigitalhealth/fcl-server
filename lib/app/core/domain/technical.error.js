"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApplicationSystemError = void 0;
class ApplicationSystemError extends Error {
    constructor(...args) {
        super(...args);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}
exports.ApplicationSystemError = ApplicationSystemError;
//# sourceMappingURL=technical.error.js.map