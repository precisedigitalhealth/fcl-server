"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserAlreadyExistsError = exports.UserNotVerifiedError = exports.UserNotActivatedError = exports.AuthorizationError = void 0;
const domain_error_1 = require("../../core/domain/domain.error");
class AuthorizationError extends domain_error_1.ApplicationDomainError {
    constructor(...args) {
        super(...args);
        this.timeToWait = 0;
    }
}
exports.AuthorizationError = AuthorizationError;
class UserNotActivatedError extends domain_error_1.ApplicationDomainError {
    constructor(...args) {
        super(...args);
    }
}
exports.UserNotActivatedError = UserNotActivatedError;
class UserNotVerifiedError extends domain_error_1.ApplicationDomainError {
    constructor(...args) {
        super(...args);
    }
}
exports.UserNotVerifiedError = UserNotVerifiedError;
class UserAlreadyExistsError extends domain_error_1.ApplicationDomainError {
    constructor(...args) {
        super(...args);
    }
}
exports.UserAlreadyExistsError = UserAlreadyExistsError;
//# sourceMappingURL=domain.error.js.map