"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createUser = void 0;
const argon2 = require("argon2");
const defaultHashOptions = {
    hashLength: 128,
    timeCost: 10,
    memoryCost: 1024,
    parallelism: 4,
    type: argon2.argon2id,
};
class GenericUser {
    constructor(id, email, fname, lname, inst, _password, dataProtectionAgreed, dataProtectionDate, newsRegAgreed, newsMailAgreed, newsDate, enabled, adminEnabled, numAttempt, lastAttempt) {
        this._password = _password;
        this.enabled = enabled;
        this.adminEnabled = adminEnabled;
        this.numAttempt = numAttempt;
        this.lastAttempt = lastAttempt;
        this.uniqueId = id;
        this.email = email;
        this.firstName = fname;
        this.lastName = lname;
        this.institution = inst;
        this.dataProtectionAgreed = dataProtectionAgreed;
        this.dataProtectionDate = dataProtectionDate;
        this.newsRegAgreed = newsRegAgreed;
        this.newsMailAgreed = newsMailAgreed;
        this.newsDate = newsDate;
    }
    static create(id, email, fname, lname, inst, password, dataProtectionAgreed, dataProtectionDate, newsRegAgreed, newsMailAgreed, newsDate, enabled = false, adminEnabled = false, numAttempt = 0, lastAttempt = Date.now()) {
        return new GenericUser(id, email, fname, lname, inst, password, dataProtectionAgreed, dataProtectionDate, newsRegAgreed, newsMailAgreed, newsDate, enabled, adminEnabled, numAttempt, lastAttempt);
    }
    get password() {
        return this._password;
    }
    getFullName() {
        return this.firstName + ' ' + this.lastName;
    }
    isVerified(verified) {
        if (!(verified === undefined)) {
            this.enabled = !!verified;
        }
        return this.enabled;
    }
    isActivated(active) {
        if (!(active === undefined)) {
            this.adminEnabled = !!active;
        }
        return this.adminEnabled;
    }
    isNewsMailAgreed(newsMailAgreed) {
        if (!(newsMailAgreed === undefined)) {
            this.newsMailAgreed = !!newsMailAgreed;
        }
        return this.newsMailAgreed;
    }
    isAuthorized(credentials) {
        return this.verifyPassword(this._password, credentials.password);
    }
    updatePassword(password) {
        return this.hashPassword(password).then((hashed) => (this._password = hashed));
    }
    updateNumberOfFailedAttempts(increment) {
        increment ? this.numAttempt++ : (this.numAttempt = 0);
    }
    updateLastLoginAttempt() {
        this.lastAttempt = Date.now();
    }
    getNumberOfFailedAttempts() {
        return this.numAttempt;
    }
    getLastLoginAttempt() {
        return this.lastAttempt;
    }
    verifyPassword(hashedPassword, password) {
        return argon2.verify(hashedPassword, password);
    }
    hashPassword(password, options = defaultHashOptions) {
        return argon2.hash(password, options);
    }
}
function createUser(id, email, fname, lname, inst, password, dataProtectionAgreed, dataProtectionDate, newsRegAgreed, newsMailAgreed, newsDate, enabled = false, adminEnabled = false, numAttempt = 0, lastAttempt = Date.now()) {
    return GenericUser.create(id, email, fname, lname, inst, password, dataProtectionAgreed, dataProtectionDate, newsRegAgreed, newsMailAgreed, newsDate, enabled, adminEnabled, numAttempt, lastAttempt);
}
exports.createUser = createUser;
//# sourceMappingURL=user.entity.js.map