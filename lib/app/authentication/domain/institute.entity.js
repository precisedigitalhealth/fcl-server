"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createInstitution = void 0;
class DefaultInstitute {
    constructor(id) {
        this.uniqueId = id;
    }
    static create(id) {
        return new DefaultInstitute(id);
    }
}
function createInstitution(id) {
    return DefaultInstitute.create(id);
}
exports.createInstitution = createInstitution;
//# sourceMappingURL=institute.entity.js.map