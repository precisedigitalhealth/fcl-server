"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenType = void 0;
var TokenType;
(function (TokenType) {
    TokenType[TokenType["RESET"] = 0] = "RESET";
    TokenType[TokenType["ACTIVATE"] = 1] = "ACTIVATE";
    TokenType[TokenType["ADMIN"] = 2] = "ADMIN";
    TokenType[TokenType["NEWSLETTER"] = 3] = "NEWSLETTER";
})(TokenType = exports.TokenType || (exports.TokenType = {}));
//# sourceMappingURL=enums.js.map