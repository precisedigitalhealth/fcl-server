"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultRegistrationService = void 0;
const aspects_1 = require("../../../aspects");
const enums_1 = require("../../core/domain/enums");
const user_entity_1 = require("../domain/user.entity");
const enums_2 = require("../domain/enums");
const institute_entity_1 = require("../domain/institute.entity");
const inversify_1 = require("inversify");
const application_types_1 = require("./../../application.types");
const domain_error_1 = require("../domain/domain.error");
let DefaultRegistrationService = class DefaultRegistrationService {
    constructor(notificationService, tokenService, configurationService, userService, instituteService) {
        this.notificationService = notificationService;
        this.tokenService = tokenService;
        this.configurationService = configurationService;
        this.userService = userService;
        this.instituteService = instituteService;
        this.appName = this.configurationService.getApplicationConfiguration().appName;
        this.apiUrl = this.configurationService.getApplicationConfiguration().apiUrl;
        this.supportContact = this.configurationService.getApplicationConfiguration().supportContact;
    }
    async verifyUser(token) {
        const userToken = await this.tokenService.getUserTokenByJWT(token);
        const userId = userToken.userId;
        this.tokenService.verifyTokenWithUser(token, String(userId));
        const user = await this.userService.getUserById(userId);
        user.isVerified(true);
        await this.userService.updateUser(user);
        await this.tokenService.deleteTokenForUser(user);
        await this.prepareUserForAdminActivation(user);
        aspects_1.logger.info(`${this.constructor.name}.${this.verifyUser.name}, User verification successful. token=${token}`);
        return user.email;
    }
    async activateUser(adminToken) {
        const userAdminToken = await this.tokenService.getUserTokenByJWT(adminToken);
        const userId = userAdminToken.userId;
        this.tokenService.verifyTokenWithUser(adminToken, String(userId));
        const user = await this.userService.getUserById(userId);
        user.isActivated(true);
        await this.userService.updateUser(user);
        await this.tokenService.deleteTokenForUser(user, enums_2.TokenType.ADMIN);
        const adminActivationNotification = this.createAdminActivationNotification(user);
        this.notificationService.sendNotification(adminActivationNotification);
        const userName = user.firstName + ' ' + user.lastName;
        aspects_1.logger.verbose(`${this.constructor.name}.${this.activateUser.name}, User activation successful.`);
        return userName;
    }
    async confirmNewsletterSubscription(newsletterToken) {
        const userNewsletterToken = await this.tokenService.getUserTokenByJWT(newsletterToken);
        const userId = userNewsletterToken.userId;
        this.tokenService.verifyTokenWithUser(newsletterToken, String(userId));
        const user = await this.userService.getUserById(userId);
        user.isNewsMailAgreed(true);
        user.newsDate = new Date();
        await this.userService.updateUser(user);
        await this.tokenService.deleteTokenForUser(user, enums_2.TokenType.NEWSLETTER);
        const userName = user.firstName + ' ' + user.lastName;
        aspects_1.logger.verbose(`${this.constructor.name}.${this.confirmNewsletterSubscription.name}, User newsletter subscription successful.`);
        return userName;
    }
    async registerUser(credentials) {
        let instituteIsUnknown = false;
        const result = await this.userService.hasUserWithEmail(credentials.email);
        if (result) {
            this.handleAlreadyRegisteredUser(credentials);
            throw new domain_error_1.UserAlreadyExistsError('Registration failed. User already exists');
        }
        let inst;
        try {
            inst = await this.instituteService.getInstituteById(credentials.institution);
        }
        catch (error) {
            aspects_1.logger.error(`${this.constructor.name}.${this.registerUser.name}, Unable to find instituton: error=${error}.`);
            aspects_1.logger.info(`${this.constructor.name}.${this.registerUser.name}, link registered user to dummy institution.`);
            instituteIsUnknown = true;
            inst = await this.getDummyInstitution();
        }
        const newUser = user_entity_1.createUser('0000', credentials.email, credentials.firstName, credentials.lastName, inst, '', credentials.dataProtectionAgreed, new Date(), credentials.newsRegAgreed, credentials.newsMailAgreed, new Date());
        await newUser.updatePassword(credentials.password);
        const user = await this.userService.createUser(newUser);
        const recoveryData = {
            userAgent: credentials.userAgent,
            email: user.email,
            host: credentials.host,
        };
        if (instituteIsUnknown) {
            const requestAdminActivationNotification = this.createRequestForUnknownInstituteNotification(user, credentials.institution);
            this.notificationService.sendNotification(requestAdminActivationNotification);
        }
        if (user.newsRegAgreed === true) {
            await this.prepareUserForNewsletterAgreement(user, recoveryData);
        }
        return this.prepareUserForVerification(user, recoveryData);
    }
    async prepareUserForVerification(user, recoveryData) {
        const hasOldToken = await this.tokenService.hasTokenForUser(user);
        if (hasOldToken) {
            await this.tokenService.deleteTokenForUser(user);
        }
        const token = this.tokenService.generateToken(user.uniqueId);
        const activationToken = await this.tokenService.saveToken(token, enums_2.TokenType.ACTIVATE, user.uniqueId);
        const requestActivationNotification = this.createRequestActivationNotification(user, recoveryData, activationToken);
        return this.notificationService.sendNotification(requestActivationNotification);
    }
    handleNotActivatedUser(user) {
        const requestNotAdminActivatedNotification = this.createNotAdminActivatedNotification(user);
        this.notificationService.sendNotification(requestNotAdminActivatedNotification);
        const requestAdminActivationReminder = this.createAdminActivationReminder(user);
        return this.notificationService.sendNotification(requestAdminActivationReminder);
    }
    async prepareUserForAdminActivation(user) {
        const hasOldAdminToken = await this.tokenService.hasTokenForUser(user, enums_2.TokenType.ADMIN);
        if (hasOldAdminToken) {
            await this.tokenService.deleteTokenForUser(user, enums_2.TokenType.ADMIN);
        }
        const adminToken = this.tokenService.generateAdminToken(user.uniqueId);
        const adminActivationToken = await this.tokenService.saveToken(adminToken, enums_2.TokenType.ADMIN, user.uniqueId);
        const requestAdminActivationNotification = this.createRequestAdminActivationNotification(user, adminActivationToken);
        return this.notificationService.sendNotification(requestAdminActivationNotification);
    }
    async prepareUserForNewsletterAgreement(user, recoveryData) {
        const hasOldNewsletterToken = await this.tokenService.hasTokenForUser(user, enums_2.TokenType.NEWSLETTER);
        if (hasOldNewsletterToken) {
            await this.tokenService.deleteTokenForUser(user, enums_2.TokenType.NEWSLETTER);
        }
        const newsletterToken = this.tokenService.generateNewsletterToken(user.uniqueId);
        const newsletterAgreementToken = await this.tokenService.saveToken(newsletterToken, enums_2.TokenType.NEWSLETTER, user.uniqueId);
        const requestNewsletterAgreementNotification = this.createRequestNewsletterAgreementNotification(user, recoveryData, newsletterAgreementToken);
        return this.notificationService.sendNotification(requestNewsletterAgreementNotification);
    }
    handleAlreadyRegisteredUser(credentials) {
        const userAlreadyRegisteredNotification = this.createAlreadyRegisteredUserNotification(credentials);
        return this.notificationService.sendNotification(userAlreadyRegisteredNotification);
    }
    async getDummyInstitution() {
        let inst;
        try {
            inst = await this.instituteService.getInstituteByName('dummy');
        }
        catch (error) {
            aspects_1.logger.warn(`Dummy institute doesn't exists: Creating! error=${error}`);
            const newInstitution = institute_entity_1.createInstitution('0000');
            newInstitution.stateShort = 'dummy';
            newInstitution.name = 'dummy';
            newInstitution.city = 'dummy';
            newInstitution.zip = 'dummy';
            newInstitution.phone = 'dummy';
            newInstitution.fax = 'dummy';
            inst = await this.instituteService.createInstitute(newInstitution);
        }
        return inst;
    }
    createRequestActivationNotification(user, recoveryData, activationToken) {
        return {
            type: enums_1.NotificationType.REQUEST_ACTIVATION,
            payload: {
                name: user.firstName + ' ' + user.lastName,
                action_url: this.apiUrl + '/users/activate/' + activationToken.token,
                api_url: this.apiUrl,
                operating_system: recoveryData.host,
                user_agent: recoveryData.userAgent,
                support_contact: this.supportContact,
                appName: this.appName,
            },
            meta: this.notificationService.createEmailNotificationMetaData(user.email, `Aktivieren Sie Ihr Konto für ${this.appName} `),
        };
    }
    createRequestAdminActivationNotification(user, adminActivationToken) {
        const fullName = user.firstName + ' ' + user.lastName;
        return {
            type: enums_1.NotificationType.REQUEST_ADMIN_ACTIVATION,
            payload: {
                name: fullName,
                action_url: this.apiUrl +
                    '/users/adminactivate/' +
                    adminActivationToken.token,
                api_url: this.apiUrl,
                email: user.email,
                institution: user.institution.name,
                location: user.institution.addendum,
                appName: this.appName,
            },
            meta: this.notificationService.createEmailNotificationMetaData(this.supportContact, `Aktivieren Sie das ${this.appName} Konto für ${fullName}`),
        };
    }
    createRequestNewsletterAgreementNotification(user, recoveryData, newsletterAgreementToken) {
        return {
            type: enums_1.NotificationType.NEWSLETTER_AGREEMENT,
            payload: {
                name: user.firstName + ' ' + user.lastName,
                action_url: this.apiUrl +
                    '/users/newsactivate/' +
                    newsletterAgreementToken.token,
                api_url: this.apiUrl,
                operating_system: recoveryData.host,
                user_agent: recoveryData.userAgent,
                support_contact: this.supportContact,
                appName: this.appName,
            },
            meta: this.notificationService.createEmailNotificationMetaData(user.email, `Agreement for newsletter subscription for ${this.appName} `),
        };
    }
    createRequestForUnknownInstituteNotification(user, institution) {
        const fullName = user.firstName + ' ' + user.lastName;
        return {
            type: enums_1.NotificationType.REQUEST_UNKNOWN_INSTITUTE,
            payload: {
                name: fullName,
                api_url: this.apiUrl,
                email: user.email,
                institution: institution,
                appName: this.appName,
            },
            meta: this.notificationService.createEmailNotificationMetaData(this.supportContact, `Aktivierungsanfrage für das ${this.appName} Konto von ${fullName} mit nicht registriertem Institut`),
        };
    }
    createAdminActivationNotification(user) {
        const fullName = user.firstName + ' ' + user.lastName;
        return {
            type: enums_1.NotificationType.NOTIFICATION_ADMIN_ACTIVATION,
            payload: {
                name: fullName,
                appName: this.appName,
            },
            meta: this.notificationService.createEmailNotificationMetaData(user.email, `Admin Aktivierung Ihres ${this.appName} Kontos`),
        };
    }
    createNotAdminActivatedNotification(user) {
        const fullName = user.firstName + ' ' + user.lastName;
        return {
            type: enums_1.NotificationType.NOTIFICATION_NOT_ADMIN_ACTIVATED,
            payload: {
                name: fullName,
                appName: this.appName,
            },
            meta: this.notificationService.createEmailNotificationMetaData(user.email, `Noch keine Admin Aktivierung Ihres ${this.appName} Kontos`),
        };
    }
    createAdminActivationReminder(user) {
        const fullName = user.firstName + ' ' + user.lastName;
        return {
            type: enums_1.NotificationType.REMINDER_ADMIN_ACTIVATION,
            payload: {
                name: fullName,
                email: user.email,
                institution: user.institution.name,
                location: user.institution.addendum,
                appName: this.appName,
            },
            meta: this.notificationService.createEmailNotificationMetaData(this.supportContact, `Erinnerung: Bitte aktivieren Sie das ${this.appName} Konto für ${fullName}`),
        };
    }
    createAlreadyRegisteredUserNotification(credentials) {
        const fullName = credentials.firstName + ' ' + credentials.lastName;
        return {
            type: enums_1.NotificationType.NOTIFICATION_ALREADY_REGISTERED,
            payload: {
                name: fullName,
                action_url: this.apiUrl + '/users/recovery',
                appName: this.appName,
            },
            meta: this.notificationService.createEmailNotificationMetaData(credentials.email, `Ihre Registrierung für ein ${this.appName} Konto`),
        };
    }
};
DefaultRegistrationService = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(application_types_1.APPLICATION_TYPES.NotificationService)),
    __param(1, inversify_1.inject(application_types_1.APPLICATION_TYPES.TokenService)),
    __param(2, inversify_1.inject(application_types_1.APPLICATION_TYPES.ConfigurationService)),
    __param(3, inversify_1.inject(application_types_1.APPLICATION_TYPES.UserService)),
    __param(4, inversify_1.inject(application_types_1.APPLICATION_TYPES.InstituteService)),
    __metadata("design:paramtypes", [Object, Object, Object, Object, Object])
], DefaultRegistrationService);
exports.DefaultRegistrationService = DefaultRegistrationService;
//# sourceMappingURL=registration.service.js.map