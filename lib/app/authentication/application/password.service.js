"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultPasswordService = void 0;
const enums_1 = require("./../domain/enums");
const enums_2 = require("../../core/domain/enums");
const inversify_1 = require("inversify");
const application_types_1 = require("./../../application.types");
let DefaultPasswordService = class DefaultPasswordService {
    constructor(notificationService, tokenService, configurationService, userService) {
        this.notificationService = notificationService;
        this.tokenService = tokenService;
        this.configurationService = configurationService;
        this.userService = userService;
        this.appName = this.configurationService.getApplicationConfiguration().appName;
        this.apiUrl = this.configurationService.getApplicationConfiguration().apiUrl;
        this.supportContact = this.configurationService.getApplicationConfiguration().supportContact;
    }
    async requestPasswordReset(recoveryData) {
        const user = await this.userService.getUserByEmail(recoveryData.email);
        const hasOldToken = await this.tokenService.hasTokenForUser(user, enums_1.TokenType.RESET);
        if (hasOldToken) {
            await this.tokenService.deleteTokenForUser(user, enums_1.TokenType.RESET);
        }
        const token = this.tokenService.generateToken(user.uniqueId);
        const resetToken = await this.tokenService.saveToken(token, enums_1.TokenType.RESET, user.uniqueId);
        const requestResetNotification = this.createResetRequestNotification(user, recoveryData, resetToken);
        return this.notificationService.sendNotification(requestResetNotification);
    }
    async resetPassword(token, password) {
        const userToken = await this.tokenService.getUserTokenByJWT(token);
        const userId = userToken.userId;
        this.tokenService.verifyTokenWithUser(token, String(userId));
        const user = await this.userService.getUserById(userId);
        await user.updatePassword(password);
        await this.userService.updateUser(user);
        await this.tokenService.deleteTokenForUser(user, enums_1.TokenType.RESET);
        const resetSuccessNotification = this.createResetSuccessNotification(user);
        return this.notificationService.sendNotification(resetSuccessNotification);
    }
    createResetRequestNotification(user, recoveryData, resetToken) {
        return {
            type: enums_2.NotificationType.REQUEST_RESET,
            payload: {
                name: user.firstName + ' ' + user.lastName,
                action_url: this.apiUrl + '/users/reset/' + resetToken.token,
                api_url: this.apiUrl,
                operating_system: recoveryData.host,
                user_agent: recoveryData.userAgent,
                support_contact: this.supportContact,
                appName: this.appName,
            },
            meta: this.notificationService.createEmailNotificationMetaData(user.email, `Setzen Sie Ihr ${this.appName}-Konto Passwort zurück.`),
        };
    }
    createResetSuccessNotification(user) {
        return {
            type: enums_2.NotificationType.RESET_SUCCESS,
            payload: {
                name: user.firstName + ' ' + user.lastName,
                api_url: this.apiUrl,
                email: user.email,
                action_url: this.apiUrl + '/users/login',
                appName: this.appName,
            },
            meta: this.notificationService.createEmailNotificationMetaData(user.email, `Passwort für ${this.appName}-Konto erfolgreich zurückgesetzt.`),
        };
    }
};
DefaultPasswordService = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(application_types_1.APPLICATION_TYPES.NotificationService)),
    __param(1, inversify_1.inject(application_types_1.APPLICATION_TYPES.TokenService)),
    __param(2, inversify_1.inject(application_types_1.APPLICATION_TYPES.ConfigurationService)),
    __param(3, inversify_1.inject(application_types_1.APPLICATION_TYPES.UserService)),
    __metadata("design:paramtypes", [Object, Object, Object, Object])
], DefaultPasswordService);
exports.DefaultPasswordService = DefaultPasswordService;
//# sourceMappingURL=password.service.js.map