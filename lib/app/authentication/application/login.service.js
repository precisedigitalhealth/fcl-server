"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultLoginService = void 0;
const moment = require("moment");
const domain_error_1 = require("./../domain/domain.error");
const inversify_1 = require("inversify");
const application_types_1 = require("./../../application.types");
let DefaultLoginService = class DefaultLoginService {
    constructor(registrationService, tokenService, configurationService, userService) {
        this.registrationService = registrationService;
        this.tokenService = tokenService;
        this.configurationService = configurationService;
        this.userService = userService;
        this.threshold = this.configurationService.getApplicationConfiguration().login.threshold;
        this.secondsDelay = this.configurationService.getApplicationConfiguration().login.secondsDelay;
        this.gdprDate = this.configurationService.getApplicationConfiguration().gdprDate;
    }
    async loginUser(credentials) {
        const user = await this.userService.getUserByEmail(credentials.email);
        if (!user.isVerified()) {
            return this.registrationService
                .prepareUserForVerification(user, {
                userAgent: credentials.userAgent,
                email: user.email,
                host: credentials.host,
            })
                .then(() => {
                throw new domain_error_1.UserNotVerifiedError(`User not verified. user=${user.email}`);
            })
                .catch((error) => {
                throw error;
            });
        }
        if (!user.isActivated()) {
            this.registrationService.handleNotActivatedUser(user);
            throw new domain_error_1.UserNotActivatedError(`User not activated. user=${user.email}`);
        }
        const diffToDelay = this.diffTimeSinceLastFailedLogin(user);
        if (this.hasToManyFailedAttempts(user) && diffToDelay >= 0) {
            const timeToWait = moment
                .duration(diffToDelay, 'seconds')
                .asSeconds();
            const error = new domain_error_1.AuthorizationError(`Too many failed attempts. user=${user}; timeToWait=${timeToWait}`);
            error.timeToWait = timeToWait;
            throw error;
        }
        if (await user.isAuthorized(credentials)) {
            let gdprAgreementRequested = true;
            if (user.getNumberOfFailedAttempts() > 0) {
                user.updateNumberOfFailedAttempts(false);
                await this.userService.updateUser(user);
            }
            await this.userService.updateUser(user);
            if (user.dataProtectionAgreed && user.dataProtectionDate) {
                moment.locale('en');
                const dateParseString = 'MMMM DD, YYYY';
                const gdprDateParsed = moment(this.gdprDate, dateParseString, true);
                const userGDPRDate = moment(user.dataProtectionDate);
                if (gdprDateParsed.isBefore(userGDPRDate)) {
                    gdprAgreementRequested = false;
                }
            }
            return {
                user: user,
                token: this.tokenService.generateToken(user.uniqueId),
                gdprAgreementRequested: gdprAgreementRequested,
            };
        }
        user.updateNumberOfFailedAttempts(true);
        user.updateLastLoginAttempt();
        await this.userService.updateUser(user);
        throw new domain_error_1.AuthorizationError(`User not authorized. user=${user.email}`);
    }
    async confirmGDPR(confirmationRequest) {
        const user = await this.userService.getUserByEmail(confirmationRequest.email);
        if (confirmationRequest.gdprConfirmed) {
            let gdprAgreementRequested = false;
            user.dataProtectionAgreed = true;
            user.dataProtectionDate = new Date();
            await this.userService.updateUser(user);
            return {
                user: user,
                token: confirmationRequest.token,
                gdprAgreementRequested: gdprAgreementRequested,
            };
        }
        throw new domain_error_1.AuthorizationError(`User not authorized. user=${user.email}`);
    }
    hasToManyFailedAttempts(user) {
        return user.getNumberOfFailedAttempts() >= this.threshold
            ? true
            : false;
    }
    diffTimeSinceLastFailedLogin(user) {
        moment.locale('de');
        const currentMoment = moment();
        const lastMoment = moment(user.getLastLoginAttempt());
        const diffToLast = moment.duration(currentMoment.diff(lastMoment));
        const diffToDelay = Math.round(this.secondsDelay - diffToLast.asSeconds());
        return diffToDelay;
    }
};
DefaultLoginService = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(application_types_1.APPLICATION_TYPES.RegistrationService)),
    __param(1, inversify_1.inject(application_types_1.APPLICATION_TYPES.TokenService)),
    __param(2, inversify_1.inject(application_types_1.APPLICATION_TYPES.ConfigurationService)),
    __param(3, inversify_1.inject(application_types_1.APPLICATION_TYPES.UserService)),
    __metadata("design:paramtypes", [Object, Object, Object, Object])
], DefaultLoginService);
exports.DefaultLoginService = DefaultLoginService;
//# sourceMappingURL=login.service.js.map