"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultTokenService = void 0;
const jsonwebtoken_1 = require("jsonwebtoken");
const enums_1 = require("../domain/enums");
const inversify_1 = require("inversify");
const application_types_1 = require("./../../application.types");
let DefaultTokenService = class DefaultTokenService {
    constructor(configurationService, tokenRepository) {
        this.configurationService = configurationService;
        this.tokenRepository = tokenRepository;
        this.expirationTime = 60 * 60 * 24;
        this.adminExpirationTime = 60 * 60 * 24 * 7;
        const serverConfig = this.configurationService.getApplicationConfiguration();
        this.jwtSecret = serverConfig.jwtSecret;
    }
    generateToken(userId) {
        const payload = {
            sub: userId,
        };
        return jsonwebtoken_1.sign(payload, this.jwtSecret, {
            expiresIn: this.expirationTime,
        });
    }
    generateAdminToken(id) {
        const payload = {
            sub: id,
            admin: true,
        };
        return jsonwebtoken_1.sign(payload, this.jwtSecret, {
            expiresIn: this.adminExpirationTime,
        });
    }
    generateNewsletterToken(id) {
        const payload = {
            sub: id,
            newsletter: true,
        };
        return jsonwebtoken_1.sign(payload, this.jwtSecret, {
            expiresIn: this.expirationTime,
        });
    }
    verifyTokenWithUser(token, id) {
        return jsonwebtoken_1.verify(token, this.jwtSecret, { subject: id });
    }
    verifyToken(token) {
        return jsonwebtoken_1.verify(token, this.jwtSecret);
    }
    saveToken(token, type, userId) {
        return this.tokenRepository.saveToken({
            token,
            type,
            userId,
        });
    }
    getUserTokenByJWT(token) {
        return this.tokenRepository.getUserTokenByJWT(token);
    }
    deleteTokenForUser(user, type = enums_1.TokenType.ACTIVATE) {
        return this.tokenRepository.deleteTokenForUser(user, type);
    }
    hasTokenForUser(user, type = enums_1.TokenType.ACTIVATE) {
        return this.tokenRepository.hasTokenForUser(user, type);
    }
};
DefaultTokenService = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(application_types_1.APPLICATION_TYPES.ConfigurationService)),
    __param(1, inversify_1.inject(application_types_1.APPLICATION_TYPES.TokenRepository)),
    __metadata("design:paramtypes", [Object, Object])
], DefaultTokenService);
exports.DefaultTokenService = DefaultTokenService;
//# sourceMappingURL=token.service.js.map