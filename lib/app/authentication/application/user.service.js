"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultUserService = void 0;
const inversify_1 = require("inversify");
const application_types_1 = require("./../../application.types");
let DefaultUserService = class DefaultUserService {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }
    getUserByEmail(email) {
        return this.userRepository.findByUsername(email);
    }
    getUserById(userId) {
        return this.userRepository.findByUserId(userId);
    }
    updateUser(user) {
        return this.userRepository.updateUser(user);
    }
    createUser(user) {
        return this.userRepository.createUser(user);
    }
    hasUserWithEmail(email) {
        return this.userRepository.hasUserWithEmail(email);
    }
};
DefaultUserService = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(application_types_1.APPLICATION_TYPES.UserRepository)),
    __metadata("design:paramtypes", [Object])
], DefaultUserService);
exports.DefaultUserService = DefaultUserService;
//# sourceMappingURL=user.service.js.map