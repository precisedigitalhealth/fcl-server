"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createApplication = void 0;
const application_types_1 = require("./application.types");
function createApplication(container) {
    const notificationService = container.get(application_types_1.APPLICATION_TYPES.NotificationService);
    return {
        addNotificationHandler: (handler) => {
            notificationService.addHandler(handler);
        },
    };
}
exports.createApplication = createApplication;
//# sourceMappingURL=application.js.map