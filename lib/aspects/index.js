"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var logging_1 = require("./logging");
Object.defineProperty(exports, "logger", { enumerable: true, get: function () { return logging_1.logger; } });
var container_1 = require("./container/container");
Object.defineProperty(exports, "getContainer", { enumerable: true, get: function () { return container_1.getContainer; } });
//# sourceMappingURL=index.js.map