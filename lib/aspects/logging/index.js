"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = exports.Logger = void 0;
const winston = require("winston");
const config = require("config");
const logConfiguration = config.get('general');
class Logger {
    constructor() {
        let logLevel = 'error';
        try {
            logLevel = logConfiguration.logLevel;
        }
        catch (err) {
            console.warn('Log Level configuration not found. Using default: ' + logLevel);
        }
        this._logger = winston.createLogger({
            level: Logger.mapLogLevels(logLevel),
            format: winston.format.combine(winston.format.colorize(), winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }), winston.format.printf((info) => Logger.mapLogMessage(info))),
            transports: [new winston.transports.Console()],
        });
    }
    static mapLogMessage(info) {
        let logMsg = `${info.timestamp} ${info.level} ${info.message}`;
        logMsg =
            info.meta !== undefined
                ? logMsg +
                    ' ' +
                    (typeof info.meta === 'object'
                        ? JSON.stringify(info.meta)
                        : info.meta)
                : logMsg;
        return logMsg;
    }
    static mapLogLevels(level) {
        switch (level) {
            case 'trace':
                return 'silly';
            case 'info':
                return level;
            case 'error':
                return level;
            case 'verbose':
                return level;
            case 'warn':
                return level;
            case 'silly':
                return level;
            case 'debug':
                return level;
            default:
                return 'info';
        }
    }
    static mapLevelToMorganFormat(level) {
        switch (level) {
            case 'trace':
                return 'dev';
            case 'info':
                return 'combined';
            case 'error':
                return 'combined';
            case 'verbose':
                return 'dev';
            case 'warn':
                return 'combined';
            case 'silly':
                return 'dev';
            case 'debug':
                return 'dev';
            default:
                return 'info';
        }
    }
    error(msg, meta) {
        this._logger.log('error', msg, { meta: meta });
    }
    warn(msg, meta) {
        this._logger.log('warn', msg, { meta: meta });
    }
    info(msg, meta) {
        this._logger.log('info', msg, { meta: meta });
    }
    verbose(msg, meta) {
        this._logger.log('verbose', msg, { meta: meta });
    }
    debug(msg, meta) {
        this._logger.log('debug', msg, { meta: meta });
    }
    trace(msg, meta) {
        this._logger.log('silly', msg, { meta: meta });
    }
}
exports.Logger = Logger;
const logger = new Logger();
exports.logger = logger;
//# sourceMappingURL=index.js.map