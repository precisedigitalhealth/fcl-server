"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getContainer = void 0;
const inversify_1 = require("inversify");
require("reflect-metadata");
function getContainer(...args) {
    return new inversify_1.Container(...args);
}
exports.getContainer = getContainer;
//# sourceMappingURL=container.js.map