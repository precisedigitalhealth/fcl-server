FROM node:latest

WORKDIR /home/fcl-server

COPY . ./

RUN npm install forever -g

EXPOSE 3000

ENTRYPOINT ["forever"]
CMD ["./lib/main.js"]
